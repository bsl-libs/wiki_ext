# Внутренняя сеть хоста для виртуальной машины Ubuntu server

## Параметры виртуальной машины

Выключаем виртуальную машину

Настраиваем параметры сети виртуальной машины

<details><summary>Скрин настроек, развернуть</summary>

![](./vbox-netsettings.png)

</details>

Запускаем виртуальную машину

## Настройка внутренней сети в Ubuntu server

Сначала надо выяснить какие вообще интерфейсы присутствуют в системе и всем ли из них назначены адреса

```
ip a
```

примерный вывод.

```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:53:37:08 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic enp0s3
       valid_lft 86239sec preferred_lft 86239sec
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e0:0e:d3 brd ff:ff:ff:ff:ff:ff
```

в данном случае наблюдаем, что на интерфейсе ```enp0s8``` назначенный адрес отсутствует. значит необходимо создать для этого интерфейса настройку и ее применить

## Настройка сетевого интерфейса

создаем файл настройки, шаблон имени 50-<имя интерфейса>.yaml

```
sudo touch /etc/netplan/50-enp0s8.yaml
```

открываем файл настройки для редактирования

```
sudo nano /etc/netplan/50-enp0s8.yaml
```

и приводим его к виду

```
network:
  ethernets:
    enp0s8:
      dhcp4: true
  version: 2
```

применяем конфигурацию

```
sudo netplan apply
```

проверяем

```
ip a
```

теперь для интерфейса ```enp0s8``` должен быть назначен адрес inet

и по этому адресу виртуальная машина доступна из базовой системы.
