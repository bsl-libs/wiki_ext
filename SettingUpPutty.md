1) [Настроить](https://gitlab.com/bsl-libs/wiki_ext/-/blob/main/notes/virtualbox-network.md) виртуалку для использования __внутренней сети__

2) Установить [Putty](https://www.putty.org) для работы с буфером обмена:
> sudo add-apt-repository universe  
> sudo apt update  
> sudo apt install -y putty

3) [Установить](https://www.cloud4y.ru/blog/how-to-install-and-configure-ssh-ubuntu/) и настроить __SSH__ в Ubuntu 22.04

4) Запускаем Putty и заполняем данные  
 ![Скрин](putty.PNG)  
В поле HostName вводим ваш Ip