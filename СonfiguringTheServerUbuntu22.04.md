## Это руководство по установке и настройке Mediawiki на Ubuntu server 22.04, необходимой для работы подсистемы Вики в 1С. ##

Первоначальная настройка Ubuntu server 22.04 - [пример](https://interface31.ru/tech_it/2022/10/linux-nachinayushhim-ustanovka-i-pervonachal-naya-nastroyka-ubuntu-2204-lts-dlya-servera.html)

##### [Установка](https://gitlab.com/bsl-libs/wiki_ext/-/blob/main/SettingUpPutty.md) дополнений для возможности использования буфера обмена через Putty ##### 

Установка __MC__ - текстовый двухпанельный файловый менеджер
> sudo apt install mc

Предполагается имеющийся сервер под управлением Ubuntu 22.04 с установленным Apache и MySQL (так называемый [LAMP server](https://icopydoc.ru/nastrojka-servera-ubuntu-22-04-lamp/)).


#### __Установка и настройка__ ####

Обновляем систему
```
 sudo apt-get update  
 sudo apt-get upgrade
```

__База данных__

Необходимо создать базу данных и пользователя, используемых в вики. Предположим, что база данных будет называться wiki, пользователь wikiuser с паролем password

Подключаемся к серверу MySQL
```
 sudo mysql -u root -p
```
Создаем базу данных  
```
 mysql> CREATE DATABASE `wiki`;  
```
Создаем пользователя, базы данных
```
 mysql> CREATE USER 'wikiuser'@'localhost' IDENTIFIED BY 'password';
```
И назначаем ему права
```
 mysql> GRANT ALL PRIVILEGES ON `wiki`.* TO 'wikiuser'@'localhost';
```
Обновляем привилегии командой
```
 mysql> FLUSH PRIVILEGES;
```
Отключаемся от сервера MySQL
```
 mysql> EXIT;
```

#### __Установка Mediawiki__ ####

Скачиваем архив Mediawiki, в статье взят последний на момент написания LTS релиз 1.39.3

Версии > 1.39 устанавливаются аналогично, необходимо изменить только ссылку на необходимый архив mediawiki

[Страница загрузки](https://www.mediawiki.org/wiki/Download)

Из командной строки
```
 cd ~  
 wget https://releases.wikimedia.org/mediawiki/1.39/mediawiki-1.39.3.tar.gz -O - | tar -xz  
 sudo mv ~/mediawiki-1.39.3 /var/www/html/mediawiki
```
__Конфигурация веб-сервера Apache__

Устанавливаем необходимые в дальнейшем пакеты
```
 sudo apt-get install composer php8.0-mbstring php8.0-xml php8.0-apcu imagemagick php8.0-imagick php8.0-intl php8.0-curl php8.0-gd php8.0-mysql  
 sudo a2enmod rewrite
```
__Создаем файл настройки виртуального каталога веб-сервера__
```
 sudo touch /etc/apache2/sites-available/mediawiki.conf  
 sudo ln -s /etc/apache2/sites-available/mediawiki.conf /etc/apache2/sites-enabled/mediawiki.conf  
 sudo nano /etc/apache2/sites-available/mediawiki.conf
```
Добавляем в него содержимое

```
<VirtualHost *:80>  
   ServerAdmin admin@your-domain.com  
   DocumentRoot /var/www/html/mediawiki/  
   ServerName your-domain.com  
   ServerAlias www.your-domain.com  
   <Directory /var/www/html/mediawiki/>  
       Options FollowSymLinks  
       AllowOverride All  
       Order allow,deny  
       allow from all  
   </Directory>  
   <Directory /var/www/html/mediawiki/images">  
       # Ignore .htaccess files  
       AllowOverride None  
       # Serve HTML as plaintext, don't execute SHTML  
       AddType text/plain .html .htm .shtml .phtml .php .php3 .php4 .php5 .php7  
       # Don't run arbitrary PHP code.  
       php_admin_flag engine off  
       # If you've other scripting languages, disable them too.  
   </Directory>  
   ErrorLog /var/log/apache2/your-domain.com-error_log  
   CustomLog /var/log/apache2/your-domain.com-access_log common  
</VirtualHost>  
```

Удаляем настройку по-умолчанию
```
 sudo rm /etc/apache2/sites-enabled/000-default.conf
```
Перезапускаем веб-сервер
```
 sudo service apache2 restart
```
#### __Первоначальная настройка__ ####

Выполняем первоначальную настройку Mediawiki, для этого в браузере переходим по адресу настраиваемого сервера
```
 http://server_name или http://server_ip_address
```
Нажимаем кнопку "Далее", отвечаем на вопросы. В целях исключения злоупотреблений возможностями редактирования рекомендуется выбирать профиль прав "Только для авторизованных редакторов".

При настройке включаем расширения
```
CategoryTree
Cite
CiteThisPage
ImageMap
InputBox
Nuke
ParserFunctions
PdfHandler
Renameuser
SpamBlacklist
VisualEditor
WikiEditor
```
Включаем возможность загрузки файлов

После инициализации будет предложено скачать файл настроек __LocalSettings.php__ который необходимо разместить в каталоге файлов Mediawiki /var/www/html/mediawiki

```
sudo nano /var/www/html/mediawiki/LocalSettings.php
```
Добавляем настройки из скачанного файла и добавляем в конец файла

```
# Speed improvements
$wgUseGzip = true;
$wgUseFileCache = true;

# Performance settings
$wgDisableCounters = true;
$wgMiserMode = true;

# IMPORTANT! fix login problem
$wgSessionCacheType = CACHE_DB;

# Add cookies lifetime
$wgCookieExpiration = 2592000;
$wgExtendedLoginCookieExpiration = 2592000;
```
Устанавливаем права на кататалог публикации, обновляем базу данных, расширения и перезапускаем веб-сервер

```
sudo chown -R www-data:www-data /var/www/html/mediawiki
cd /var/www/html/mediawiki
sudo php /var/www/html/mediawiki/maintenance/update.php
sudo composer update
sudo service apache2 restart
```
Mediawiki установлен

#### __Расширения, необходимые для работы 1С wiki__ ####

Устанавливаем [Semantic mediawiki](https://www.semantic-mediawiki.org/wiki/Semantic_MediaWiki)

```
cd /var/www/html/mediawiki
sudo composer update
sudo nano ./composer.local.json
```
Добавялем в него код
```
{
	"require": {
                  "mediawiki/semantic-media-wiki": "^4.1.1"
        }
}
```
Далее
```
sudo composer update
```
Далее добавляем в конец файла __sudo nano ./LocalSettings.php__
```
wfLoadExtension( 'SemanticMediaWiki' );
enableSemantics( 'localhost' );
```
Далее
```
sudo php maintenance/update.php
```
__Устанавливаем необходимые расширения__

```
sudo nano ./compser.json
```
```
{
    "require": {
        "data-values/common": "^1.1.0"
    }
}
```
Далее
```
sudo composer update
```
_______________________________________________________

```
sudo nano ./compser.json
```
```
{
    "require": {
        "data-values/geo": "^4.4.0"
    }
}
```
Далее
```
sudo composer update
```

sudo composer require data-values/common
sudo composer require data-values/geo
sudo composer -W require mediawiki/semantic-breadcrumb-links:~2.1.x-dev
sudo composer require mediawiki/semantic-extra-special-properties:~3.0.4
sudo composer require mediawiki/semantic-meta-tags:~3.x-dev
sudo composer require mediawiki/semantic-result-formats
```
Для включения расширений Semantics в файл /var/www/html/mediawiki/LocalSettings.php добавляем строку
```
wfLoadExtension( 'SemanticResultFormats' );
wfLoadExtension( 'SemanticMetaTags' );
wfLoadExtension( 'SemanticExtraSpecialProperties' );
wfLoadExtension( 'SemanticBreadcrumbLinks' );
wfLoadExtension( 'SemanticMediaWiki' );
enableSemantics( 'localhost' );
```

Список включенных расширений возможно посмотреть на странице __Special:Version__ вашей вики.

#### __Дополнительные расширения__ ####

__Желательные к наличию__

__VisualEditor__ - удобный визуальный редактор текстов

Для его работы необходим установленный __Parsoid__

```
sudo apt-key advanced --keyserver keys.gnupg.net --recv-keys 90E9F83F22250DD7
sudo apt-add-repository "deb https://releases.wikimedia.org/debian jessie-mediawiki main"
sudo apt-get install apt-transport-https
sudo apt-get update && sudo apt-get install parsoid
```

В файле настроек Parsiod

#!

/etc/mediawiki/parsoid/config.yaml
, необходимо указать адрес api.php нашего сервера вики.
В секции

#!

        # Configure Parsoid to point to your MediaWiki instances.
        mwApis:
        - # This is the only required parameter,
          # the URL of you MediaWiki API endpoint.

          uri: 'http://localhost/w/api.php'

          # The "domain" is used for communication with Visual Editor
          # and RESTBase.  It defaults to the hostname portion of
          # the `uri` property below, but you can manually set it
          # to an arbitrary string.
          domain: 'localhost'  # optional
          # To specify a proxy (or proxy headers) specific to this prefix
          # (which overrides defaultAPIProxyURI). Alternatively, set `proxy`
          # to `null` to override and force no proxying when a default proxy
          # has been set.
          #proxy:
          #    uri: 'http://my.proxy:1234/'
          #    headers:  # optional
          #        'X-Forwarded-Proto': 'https'
устанавливаем значение параметра uri в значение
#!

'http://server_name_or_ip/api.php'
Устанавливаем VisualEditor

#!

cd /var/www/html/mediawiki/extensions
sudo git clone -b REL1_27 https://gerrit.wikimedia.org/r/p/mediawiki/extensions/VisualEditor.git
cd VisualEditor
sudo git submodule update --init
Для включения расширения в вики в файл /var/www/html/mediawiki/LocalSettings.php добавляем строки

#!

wfLoadExtension( 'VisualEditor' );

// Enable by default for everybody
$wgDefaultUserOptions['visualeditor-enable'] = 1;

// Optional: Set VisualEditor as the default for anonymous users
// otherwise they will have to switch to VE
// $wgDefaultUserOptions['visualeditor-editor'] = "visualeditor";

// Don't allow users to disable it
$wgHiddenPrefs[] = 'visualeditor-enable';

// OPTIONAL: Enable VisualEditor's experimental code features
// $wgDefaultUserOptions['visualeditor-enable-experimental'] = 1;
Там же, в LocalSettings.php указываем точку доступа редактора к модулю Parsoid

#!

$wgVirtualRestConfig['modules']['parsoid'] = array(
    // URL to the Parsoid instance
    // Use port 8142 if you use the Debian package
    'url' => 'http://server_name_or_ip:8142',
    // Parsoid "domain", see below (optional)
    'domain' => 'localhost',
    // Parsoid "prefix", see below (optional)
    // 'prefix' => 'localhost'
);
Изменяем владельца файлов веб
#!

sudo chown www-data:www-data -R /var/www
И перезапускаем сервисы

sudo service parsoid restart
sudo service apache2 restart
Необязательные расширения

CategoryTree - дерево категорий

FlaggedRevs - возможность пометки корректности правок

UserMerge - объединение, удаление пользователей

Popups - всплывающие окна предпросмотра страниц при наведении на ссылку
