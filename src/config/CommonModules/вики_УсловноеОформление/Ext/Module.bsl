﻿
// Возвращает новый элемент условного оформления
//
// Параметры:  
// 	Оформление
// 	Имя
// 	Использование
//
// Возвращаемое значение: 
// 	ЭлементУсловногоОформления
//
Функция ЭлементДобавить(Оформление, Представление = Неопределено, Использование = Истина) Экспорт
	
	Результат	= Оформление.Элементы.Добавить();
	Результат.Представление	= Представление;
	Результат.Использование	= Использование;
	
	Возврат Результат;
	
КонецФункции //ЭлементДобавить 