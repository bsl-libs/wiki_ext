﻿// Обращения в внешним относительно расширения модулям
// 

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает общий модуль ом_Строка
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_Строка() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_Строка();
	
КонецФункции // ом_Строка       

// Возвращает общий модуль ом_ОбъектПрикладной
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_ОбъектПрикладной() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_ОбъектПрикладной();
	
КонецФункции // ом_Строка    ПеречислениеЭто

// Возвращает общий модуль ом_ТипИмя
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_ТипИмя() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_ТипИмя();
	
КонецФункции // ом_ТипИмя 

// Возвращает общий модуль ом_Тип
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_Тип() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_Тип();
	
КонецФункции // ом_Тип

// Возвращает общий модуль ом_Значение
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_Значение() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_Значение();
	
КонецФункции // ом_Значение     

// Возвращает общий модуль ом_АдаптерАктивный
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_АдаптерАктивный() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_АдаптерАктивный();
	
КонецФункции // ом_АдаптерАктивный      

// Возвращает общий модуль лг_Логгер
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция лг_Логгер() Экспорт
	
	Возврат вики_МодульПовтИсп.лг_Логгер();
	
КонецФункции // лг_Логгер     

// Возвращает общий модуль лг_Лог
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция лг_Лог() Экспорт
	
	Возврат вики_МодульПовтИсп.лг_Лог();
	
КонецФункции // лг_Лог

// Возвращает общий модуль ом_Диалог
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_Диалог() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_Диалог();
	
КонецФункции // ом_Диалог

// Возвращает общий модуль ом_ДиалогКлиент
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_ДиалогКлиент() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_ДиалогКлиент();
	
КонецФункции // ом_ДиалогКлиент

// Возвращает общий модуль ом_Метаданные
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_Метаданные() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_Метаданные();
	
КонецФункции // ом_Метаданные   

// Возвращает общий модуль ом_Версия
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_Версия() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_Версия();
	
КонецФункции // ом_Версия    

// Возвращает общий модуль ом_Коллекция
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_Коллекция() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_Коллекция();
	
КонецФункции // ом_Коллекция    

// Возвращает общий модуль ом_ОписаниеТипов
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_ОписаниеТипов() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_ОписаниеТипов();
	
КонецФункции // ом_ОписаниеТипов    

// Возвращает общий модуль ом_Преобразование
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_Преобразование() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_Преобразование();
	
КонецФункции // ом_Преобразование    

// Возвращает общий модуль ом_ДиалогКлиент8_3
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_ДиалогКлиент8_3() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_ДиалогКлиент8_3();
	
КонецФункции // ом_ДиалогКлиент8_3    

// Возвращает общий модуль ом_Адаптер
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_Адаптер() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_Адаптер();
	
КонецФункции // ом_Адаптер    

// Возвращает общий модуль кнр_Адрес
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция кнр_Адрес() Экспорт
	
	Возврат вики_МодульПовтИсп.кнр_Адрес();
	
КонецФункции // кнр_Адрес  

// Возвращает общий модуль кнр_АдресСостав
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция кнр_АдресСостав() Экспорт
	
	Возврат вики_МодульПовтИсп.кнр_АдресСостав();
	
КонецФункции // кнр_АдресСостав  

// Возвращает общий модуль ом_АдаптерАктивныйСервер
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_АдаптерАктивныйСервер() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_АдаптерАктивныйСервер();
	
КонецФункции // ом_АдаптерАктивныйСервер      

// Возвращает общий модуль ом_ФоновоеЗадание
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_ФоновоеЗадание() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_ФоновоеЗадание();
	
КонецФункции // ом_ФоновоеЗадание     

// Возвращает общий модуль ом_ФоновоеЗадание
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_СредаВыполнения() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_СредаВыполнения();
	
КонецФункции // ом_СредаВыполнения      

// Возвращает общий модуль ом_ПрогрессДиалог
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_ПрогрессДиалог() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_ПрогрессДиалог();
	
КонецФункции // ом_ПрогрессДиалог   

// Возвращает общий модуль ом_АдаптерВид
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_АдаптерВид() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_АдаптерВид();
	
КонецФункции // ом_АдаптерВид    

// Возвращает общий модуль ом_АдаптерОбъект
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_АдаптерОбъект() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_АдаптерОбъект();
	
КонецФункции // ом_АдаптерОбъект  

// Возвращает общий модуль ом_ПрогрессПараметры
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_ПрогрессПараметры() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_ПрогрессПараметры();
	
КонецФункции // ом_ПрогрессПараметры   

// Возвращает общий модуль ом_Прогресс
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_Прогресс() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_Прогресс();
	
КонецФункции // ом_Прогресс   

// Возвращает общий модуль ом_Подсистема
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_Подсистема() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_Подсистема();
	
КонецФункции // ом_Подсистема    

// Возвращает общий модуль ом_Число
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_Число() Экспорт
	
	Возврат вики_МодульПовтИсп.ом_Число();
	
КонецФункции // ом_Число    

#КонецОбласти