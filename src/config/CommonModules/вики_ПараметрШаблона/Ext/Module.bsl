﻿//
// Параметры шаблонов
// 

//
// Лог
// 

// Возвращает имя объекта лога
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ЛогОбъектИмя()
	
	Возврат "ОМ.вики_ПараметрШаблона";
	
КонецФункции //ЛогОбъектИмя 

//
// Значения параметров шаблонов
// 

// Возвращает объявление параметра шаблона
//
// Параметры: 
// 	Имя
//
// Возвращаемое значение: 
// 	Срока
//
Функция Объявление(Имя) Экспорт
	
	Возврат "
	||" + Имя + "=";
	
КонецФункции //Объявление 

// Возвращает наличие использования параметра в тексте шаблона
//
// Параметры: 
// 	Текст
// 	Имя
//
// Возвращаемое значение: 
// 	Булево
//
Функция Используется(Текст, Имя) Экспорт
	
	Возврат вики_Модуль.ом_Строка().ПодстрокаЕсть(Текст, "{{{" + Имя + "|");
	
КонецФункции //Используется 

// Возвращает значение параметра шаблона. Обрезает незначимые символы справа и удаляет html комментарии,
// содержащиеся в тексте значения параметра
// 
// Параметры:
// 	Текст
// 	Имя
// 
// Возвращаемое значение
// 	Строка
// 
Функция Получить(Текст, Имя) Экспорт
	
	Результат	= "";
	Позиция		= 0;
	Искомое		= Объявление(Имя);
	
	Если вики_Модуль.ом_Строка().ПодстрокаЕсть(Текст, Искомое, Позиция) Тогда
		Результат	= СокрП(вики_Модуль.ом_Строка().ЭлементУдалить(Сред(Текст, Позиция + СтрДлина(Искомое))
		, вики_Текст.КомментарийНачало()
		, вики_Текст.КомментарийОкончание()));
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции //Получить

// Устанавливает значение параметра шаблона. 
//
// Параметры: 
// 	РазделТекст
// 	Имя
// 	Значение
//
Процедура Установить(Текст, Имя, Значение) Экспорт
	
	Искомое	= Объявление(Имя);
	Позиция	= 0;
	
	Если вики_Модуль.ом_Строка().ПодстрокаЕсть(Текст, Искомое, Позиция) Тогда
		// Заменяет старое значение параметра на новое
		РазделТекст		= Лев(Текст, Позиция - 1)
		+ Искомое
		+ Значение
		+ Сред(Текст, Позиция + СтрДлина(Искомое) + СтрДлина(Получить(Текст, Имя)));
	КонецЕсли;
	
КонецПроцедуры //Установить 

// Возвращает текст устанавливающий значение параметра шаблона
//
// Параметры: 
// 	Имя
// 	Значение
//
// Возвращаемое значение: 
// 	Строка
//
Функция Создать(Имя, Значение) Экспорт
	
	Возврат Объявление(Имя) + Значение;
	
КонецФункции //Создать 

// Возвращает имя блока параметра шаблона
//
// Параметры: 
// 	Параметр
// 	Блок
//
// Возвращаемое значение: 
// 	Строка
//
Функция БлокИмя(Параметр, Блок = Неопределено) Экспорт
	
	Возврат ?(Блок = Неопределено ИЛИ ПустаяСтрока(Блок) ИЛИ Блок = Параметр, Параметр, Блок + "." + Параметр);
	
КонецФункции //БлокИмя 

//
// Имена параметров
// 

// Возвращает имя параметра Версия конструктора
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ВерсияКонструктораИмя() Экспорт
	
	Возврат "Версия конструктора";
	
КонецФункции //ВерсияКонструктораИмя 

// Возвращает имя параметра Заголовок
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ЗаголовокИмя() Экспорт
	
	Возврат "Заголовок";
	
КонецФункции //ЗаголовокИмя 

// Возвращает имя параметра Текст
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ТекстИмя() Экспорт
	
	Возврат "Текст";
	
КонецФункции //ТекстИмя 

// Возвращает имя параметра Владелец имя
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ВладелецИмя() Экспорт
	
	Возврат "Владелец";
	
КонецФункции //ВладелецИмя

// Возвращает имя параметра Владелец представление
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ВладелецПредставлениеИмя() Экспорт
	
	Возврат "Владелец представление";
	
КонецФункции //ВладелецПредставлениеИмя 

// Возвращает имя параметра шаблона Вид документа
//
// Параметры: 
//
// Возвращаемое значение: 
//  Строка
//
Функция ВидыДокументаИмя() Экспорт
	
	Возврат "Виды документа";
	
КонецФункции //ВидыДокументаИмя 

// Возвращает имя параметра шаблона Виды объекта
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ВидыОбъектаИмя() Экспорт
	
	Возврат "Виды объекта";
	
КонецФункции //ВидыОбъектаИмя 

// Возвращает имя параметра шаблона Иерархия
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ИерархияИмя() Экспорт
	
	Возврат "Иерархия";
	
КонецФункции //ИерархияИмя 

// Возвращает имя параметра шаблона Подчинен
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ПодчиненИмя() Экспорт
	
	Возврат "Подчинен";
	
КонецФункции //ПодчиненИмя 

// Возвращает имя параметра шаблона Назначение
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция НазначениеИмя() Экспорт
	
	Возврат "Назначение";
	
КонецФункции //НазначениеИмя 

// Возвращает имя параметра шаблона Описание
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ОписаниеИмя() Экспорт
	
	Возврат "Описание";
	
КонецФункции //ОписаниеИмя 

// Возвращает имя параметра шаблона Заполнение
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ЗаполнениеИмя() Экспорт
	
	Возврат "Заполнение";
	
КонецФункции //ЗаполнениеИмя 

// Возвращает имя параметра шаблона Реквизиты
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция РеквизитыИмя() Экспорт
	
	Возврат "Реквизиты";
	
КонецФункции //РеквизитыИмя 

// Возвращает имя параметра шаблона Реквизит
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция РеквизитИмя() Экспорт
	
	Возврат "Реквизит";
	
КонецФункции //РеквизитИмя 

// Возвращает имя параметра шаблона Табличные части
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ТабличныеЧастиИмя() Экспорт
	
	Возврат "Табличные части";
	
КонецФункции //ТабличныеЧастиИмя 

// Возвращает имя параметра шаблона Табличная часть
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ТабличнаяЧастьИмя() Экспорт
	
	Возврат "Табличная часть";
	
КонецФункции //ТабличнаяЧастьИмя 

// Возвращает имя параметра шаблона Движения документа
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ДвиженияДокументаИмя() Экспорт
	
	Возврат "Движения документа";
	
КонецФункции //ДвиженияДокументаИмя 

// Возвращает имя параметра шаблона Категории
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция КатегорииИмя() Экспорт
	
	Возврат "Категории";
	
КонецФункции //КатегорииИмя 

// Возвращает имя параметра шаблона Имя
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ИмяИмя() Экспорт
	
	Возврат "Имя";
	
КонецФункции //ИмяИмя 

// Возвращает имя параметра шаблона Представление
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ПредставлениеИмя() Экспорт
	
	Возврат "Представление";
	
КонецФункции //ПредставлениеИмя 

// Возывращает имя параметра шаблона Идентификатор
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ИдентификаторИмя() Экспорт
	
	Возврат "Идентификатор";
	
КонецФункции //ИдентификаторИмя 

// Возвращает имя параметра шаблоа Тип
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ТипИмя() Экспорт
	
	Возврат  "Тип";
	
КонецФункции //ТипИмя 

// Возвращает имя параметра шаблона Ссылка
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция СсылкаИмя() Экспорт
	
	Возврат "Ссылка";
	
КонецФункции //СсылкаИмя 

// Возвращает имя параметра шаблона Объект
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ОбъектИмя() Экспорт
	
	Возврат "Объект";
	
КонецФункции //ОбъектИмя 

// Возвращает имя параметра шаблона Состав
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция СоставИмя() Экспорт
	
	Возврат "Состав";
	
КонецФункции //СоставИмя 

// Возвращает имя параметра шаблона Значения перечисления
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ЗначенияПеречисления() Экспорт
	
	Возврат "Значения перечисления";
	
КонецФункции //ЗначенияПеречисления 

// Возвращает имя параметра шаблона Формировать заголовок
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ФормироватьЗаголовокИмя() Экспорт
	
	Возврат "Формировать заголовок";
	
КонецФункции //ФормироватьЗаголовокИмя 

// Возвращает имя параметра шаблона Уровень заголовка
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция УровеньЗаголовкаИмя() Экспорт
	
	Возврат "Уровень заголовка";
	
КонецФункции //УровеньЗаголовкаИмя 

// Возвращает имя параметра Вводится на основании
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ВводитсяНаОснованииИмя() Экспорт
	
	Возврат "Вводится на основании";
	
КонецФункции //ВводитсяНаОснованииИмя 

// Возвращает имя параметра шаблона Цвет закладки
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ЦветЗакладкиИмя() Экспорт
	
	Возврат "Цвет закладки";
	
КонецФункции //ЦветЗакладкиИмя 

// Возвращает имя параметра шаблона Цвет фона
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ЦветФонаИмя() Экспорт
	
	Возврат "Цвет фона";
	
КонецФункции //ЦветФонаИмя 

// Возвращает имя параметра шаблона Изображение
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ИзображениеИмя() Экспорт
	
	Возврат "Изображение";
	
КонецФункции //ИзображениеИмя 

// Возвращает имя параметра шаблона Изображение подсказка
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ИзображениеПодсказкаИмя() Экспорт
	
	Возврат "Изображение подсказка";
	
КонецФункции //ИзображениеПодсказкаИмя 

// Возвращает имя параметра шаблона Ширина
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ШиринаИмя() Экспорт
	
	Возврат "Ширина";
	
КонецФункции //ШиринаИмя 

// Возвращает имя параметра шаблона Положение
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ПоложениеИмя() Экспорт
	
	Возврат "Положение";
	
КонецФункции //ПоложениеИмя 

// Возвращает имя параметра шаблона Выравнивание текста
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ВыравниваниеТекстаИмя() Экспорт
	
	Возврат "Выравнивание текста";
	
КонецФункции //ВыравниваниеТекстаИмя 

// Возвращает имя параметра шаблона Страница
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция СтраницаИмя() Экспорт
	
	Возврат "Страница";
	
КонецФункции //СтраницаИмя 