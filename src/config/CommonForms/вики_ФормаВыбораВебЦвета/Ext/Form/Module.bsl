﻿
// Предопределенный метод
// 
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Заполняем таблицу цветов по веб цветам
	Для Каждого Цвет Из вики_Модуль.ом_Коллекция().ЦветаВебЗначения() Цикл
		
		Имя			= Цвет.Ключ;
		Значение	= Цвет.Значение;
		
		ЦветаСтрока	= ЦветаТаблица.Добавить();
		ЦветаСтрока.Имя			= Имя;
		ЦветаСтрока.Значение	= Значение;
		Оформление	= вики_УсловноеОформление.ЭлементДобавить(ЭтаФорма.УсловноеОформление, "Цвет" + Имя);
		вики_ЭлементУсловногоОформления.ПолеДобавить(Оформление, "ЦветаТаблицаЦвет");
		вики_ЭлементУсловногоОформления.ОтборЭлементДобавить(Оформление, "ЦветаТаблица.Значение", Значение);
		вики_ЭлементУсловногоОформления.ЦветФона(Оформление, вики_Диалог.ЦветСоздать(Значение));
	КонецЦикла;
	
КонецПроцедуры

// По команде Выбрать
// 
&НаКлиенте
Процедура Выбрать(Команда)
	
	ЭтаФорма.Закрыть(?(Элементы.ЦветаТаблица.ТекущиеДанные <> Неопределено
	, Элементы.ЦветаТаблица.ТекущиеДанные.Имя
	, Неопределено));
	
КонецПроцедуры

// Выбор цвета в таблице
// 
&НаКлиенте
Процедура ЦветаТаблицаВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	Выбрать(Неопределено);
	
КонецПроцедуры

