﻿
// Предопределенный метод
// 
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	вики_ДиалогСервер.ФормаАдминистративнаяПриСоздании(ЭтаФорма, Отказ, СтандартнаяОбработка);
	
	Если НЕ Отказ Тогда
		вики_ДиалогСервер.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
		ИнформацияЗаполнитьСервер();
	КонецЕсли;
	
КонецПроцедуры

// Устанавливает текст ошибки
//
// Параметры: 
// 	Текст
//
&НаСервере 
Процедура ОшибкаТекстУстановить(Текст)
	
	Элементы.ТекстОшибка.Заголовок		= Текст;
	Элементы.ТекстОшибка.Видимость		= НЕ ПустаяСтрока(Текст);
	Элементы.ОшибкаПереход.Видимость	= Элементы.ТекстОшибка.Видимость;
	Элементы.ОшибкаПереход.Заголовок	= ?(СоединениеЕсть, "Перейти", "Обновить");
	
КонецПроцедуры //ОшибкаТекстУстановить 

// Устанавливает параметры элементов формы
//
// Параметры: 
//
&НаСервере 
Процедура ЭлементыПараметрыУстановить()
	
	Элементы.ФормаИнициализацияОткрыть.Видимость	= НЕ СайтИнициализирован И СоединениеЕсть;
	Элементы.ФормаГруппаОбслуживание.Видимость		= СайтИнициализирован;
	Элементы.ФормаБаннерыКатегорийОткрыть.Видимость	= СайтИнициализирован;
	Элементы.ФормаФайлыУправление.Видимость			= СайтИнициализирован;
	
КонецПроцедуры //ЭлементыПараметрыУстановить 

// Заполнение информации о сайте, настройках
//
// Параметры: 
//
&НаСервере 
Процедура ИнформацияЗаполнитьСервер()
	
	НастройкиЕсть		= Ложь;
	СайтИнициализирован	= Ложь;
	ОбновлениеЕсть		= Ложь;
	
	// Настройки
	Соединение	= ИнформацияНастройкиЗаполнитьСервер();
	
	// Сайт
	Если СоединениеЕсть Тогда
		ИнформацияСайтЗаполнитьСервер(Соединение);
	КонецЕсли;
	
	ЭлементыПараметрыУстановить();
	
КонецПроцедуры //ИнформацияЗаполнить 

// Заполняет информацию о настройках
//
// Параметры: 
// 
// Возвращаемое значение
// 	Структура	- описание соединения с сайтом
//
&НаСервере 
Функция ИнформацияНастройкиЗаполнитьСервер()
	
	ОшибкаТекст		= "";
	Настройки		= вики_НастройкиСервер.Получить();
	СоединениеЕсть	= Ложь;
	Результат		= Неопределено;
	
	НастройкиЕсть		= вики_НастройкиСервер.НастройкаЕсть(Настройки);
	АдресСтрока			= вики_НастройкиСервер.АдресСтрока(Настройки);
	
	Если НЕ НастройкиЕсть Тогда
		ОшибкаТекст	= "Не настроены параметры соединения с сайтом.";
	Иначе
		Элементы.НастройкиЕсть.ЦветТекстаЗаголовка	= ЦветаСтиля.ЦветТекстаФормы;
		
		Результат	= вики_Сайт.СоединениеСоздать(Настройки);
		ОшибкаТекст	= вики_Интернет.СоединениеОшибкаТекст(Результат);
		
		Если ПустаяСтрока(ОшибкаТекст) Тогда
			СоединениеЕсть	= Истина;
		КонецЕсли;
	КонецЕсли;
	
	ОшибкаТекстУстановить(ОшибкаТекст);
	Элементы.ГруппаСайт.Доступность	= СоединениеЕсть;
	
	Возврат Результат;
	
КонецФункции //ИнформацияНастройкиЗаполнитьСервер 

// Добавляет строку таблицы расширений сайта
//
// Параметры: 
// 	Имя
// 	Обязательное
//
&НаСервере 
Процедура СайтРасширенияТаблицаСтрокаДобавить(Имя, Обязательное)
	
	ТаблицаСтрока	= СайтРасширенияТаблица.Добавить();
	ТаблицаСтрока.Имя			= Имя;
	ТаблицаСтрока.Обязательное	= Обязательное;
	
КонецПроцедуры //СайтРасширенияТаблицаСтрокаДобавить 

// Заполнение таблицы расширений сайта
//
// Параметры: 
//
&НаСервере 
Процедура СайтРасширенияТаблицаЗаполнить()
	
	СайтРасширенияТаблица.Очистить();
	
	СайтРасширенияТаблицаСтрокаДобавить("ParserFunctions", Истина);
	СайтРасширенияТаблицаСтрокаДобавить("SemanticMediaWiki", Истина);
	СайтРасширенияТаблицаСтрокаДобавить("SemanticBreadcrumbLinks", Истина);
	СайтРасширенияТаблицаСтрокаДобавить("SemanticExtraSpecialProperties", Истина);
	СайтРасширенияТаблицаСтрокаДобавить("SemanticMetaTags", Истина);
	СайтРасширенияТаблицаСтрокаДобавить("SemanticResultFormats", Истина);
	СайтРасширенияТаблицаСтрокаДобавить("CategoryTree", Ложь);
	СайтРасширенияТаблицаСтрокаДобавить("Flagged Revisions", Ложь);
	СайтРасширенияТаблицаСтрокаДобавить("UserMerge", Ложь);
	СайтРасширенияТаблицаСтрокаДобавить("Popups", Ложь);
	
КонецПроцедуры //СайтРасширенияТаблицаЗаполнить 

// Заполняет информацию о сайте
//
// Параметры: 
//
&НаСервере 
Процедура ИнформацияСайтЗаполнитьСервер(Соединение)
	
	СайтВерсия			= вики_Интернет.СоединениеПараметрыВикиВерсияПолучить(Соединение);
	СайтИнициализирован	= Ложь;
	ОшибкаТекст			= "";
	
	// Расширения
	СайтРасширенияТаблицаЗаполнить();
	Таблица	= вики_Сайт.РасширенияТаблица(Соединение);
	Для Каждого РасширенияСтрока Из СайтРасширенияТаблица Цикл
		ТаблицаСтрока	= Таблица.Найти(НРег(РасширенияСтрока.Имя), "Индекс");
		Если ТаблицаСтрока <> Неопределено Тогда
			РасширенияСтрока.Установлено	= Истина;
			РасширенияСтрока.Версия			= ТаблицаСтрока.Версия;
			РасширенияСтрока.Ссылка			= ТаблицаСтрока.Ссылка;
		КонецЕсли;
	КонецЦикла;
	Если СайтРасширенияТаблица.НайтиСтроки(Новый Структура("Установлено, Обязательное", Ложь, Истина)).Количество() Тогда
		ОшибкаТекст	= "На сайте установлены не все обязательные расширения! Отображение и взаимосвязи страниц могут работать некорректно.";
	КонецЕсли;
	
	// Сайт инициализирован ? Проверяем по наличию дерева категорий
	Если ПустаяСтрока(ОшибкаТекст) Тогда
		КатегорииДерево	= вики_Сайт.КатегорииДеревоПолучить(Соединение);
		Если КатегорииДерево.Строки.Количество() Тогда
			СайтИнициализирован	= Истина;
			ОшибкаТекст	= Соединение.ОшибкаТекст;
		Иначе
			ОшибкаТекст	= "Сайт не содержит необходимых структур. Необходима инициализация.";
		КонецЕсли;
	КонецЕсли;
	
	// ? необходимо обновление страниц
	Если СайтИнициализирован И ПустаяСтрока(ОшибкаТекст) Тогда
		Версия	= вики_Сайт.КонструкторВерсия(Соединение);
		Если Версия = Неопределено 
			ИЛИ вики_Модуль.ом_Версия().Сравнить(вики_Подсистема.КонструкторВерсия(), Версия) > 0 
			Тогда
			ОбновлениеЕсть	= Истина;
			ОшибкаТекст		= "Версия конструктора Вики информационной базы старше версии используемой на сайте. Необходимо обновление статей.";
		КонецЕсли;
	КонецЕсли;
	
	ОшибкаТекстУстановить(ОшибкаТекст);
	
КонецПроцедуры //ИнформацияСайтЗаполнитьСервер 

// Очистка кэша сайта
//
// Параметры: 
//
&НаСервере 
Функция КэшОчиститьСервер()
	
	Соединение	= вики_Сайт.СоединениеПолучить();
	Результат	= Ложь;
	
	Если ПустаяСтрока(Соединение.ОшибкаТекст) Тогда
		Результат	= вики_Сайт.КэшОчистить(Соединение);
	КонецЕсли;
	
	Если НЕ ПустаяСтрока(Соединение.ОшибкаТекст) Тогда
		Сообщение	= Новый СообщениеПользователю;
		Сообщение.Текст = Соединение.ОшибкаТекст;
		Сообщение.Сообщить(); 
		Соединение.ОшибкаТекст	= "";
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции //КэшОчиститьСервер 

// При поступлении оповещения
// 
&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = вики_Диалог.СобытиеНастройкиПриИзмененииИмя()
		ИЛИ ИмяСобытия = вики_Диалог.СобытиеСайтСтруктураПриИзмененииИмя()
		Тогда
		ИнформацияЗаполнитьСервер();
	КонецЕсли;
	
КонецПроцедуры

// По кнопке Открыть поля наличия настроек
// 
&НаКлиенте
Процедура НастройкиЕстьОткрытие(Элемент, СтандартнаяОбработка)
	
	НастройкиОткрыть(Неопределено);
	
КонецПроцедуры

// По команде настрокиОткрыть
// 
&НаКлиенте
Процедура НастройкиОткрыть(Команда)
	
	вики_ДиалогКлиент.ФормаНастройкиОткрыть(ЭтаФорма);
	
КонецПроцедуры

// По команде ПользователиСайта
// 
&НаКлиенте
Процедура ПользователиАвторизация(Команда)
	
	вики_ДиалогКлиент.ФормаПользователиАвторизацияОткрыть(ЭтаФорма);
	
КонецПроцедуры

// По команде ПраваПользователей
// 
&НаКлиенте
Процедура ПользователиИБРоли(Команда)
	
	вики_ДиалогКлиент.ФормаПраваПользователейОткрыть(ЭтаФорма);
	
КонецПроцедуры

// По коменде открытия формы инициализации
// 
&НаКлиенте
Процедура ИнициализацияОткрыть(Команда)
	
	вики_ДиалогКлиент.ФормаИнициализацияОткрыть(ЭтаФорма);
	
КонецПроцедуры

// По команде замера производительности обмена с сайтом
// 
&НаКлиенте
Процедура ПризводительностьЗамерить(Команда)
	
	вики_ДиалогКлиент.ФормаЗамерВремениОбменаОткрыть(ЭтаФорма);
	
КонецПроцедуры

// При нажатии на Имя таблицы расширений сайта
// 
&НаКлиенте
Процедура СайтРасширенияТаблицаВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ДанныеТекущие	= Элементы.СайтРасширенияТаблица.ТекущиеДанные;
	Если ДанныеТекущие <> Неопределено 
		И Поле.Имя = "СайтРасширенияТаблицаИмя" Тогда
		вики_ДиалогКлиент.ФормаБраузерОткрыть(?(ПустаяСтрока(ДанныеТекущие.Ссылка)
		, "https://www.google.ru/search?q=" + вики_Текст.ВURL("Mediawiki extension " + ДанныеТекущие.Имя)
		, ДанныеТекущие.Ссылка));
	КонецЕсли;
	
КонецПроцедуры

// Предопределенный метод
// 
&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	вики_Артефакты.ФормаОкноКнопкиПараметрыУстановить(ЭтаФорма);
	
КонецПроцедуры

// При нажатии динамической кнопки вики
//
// Параметры:
// 	Элемент
//
&НаКлиенте
Процедура Подключаемый_вики_КнопкаПриНажатии(Элемент)
	
	вики_ДиалогКлиент.КнопкаПриНажатии(ЭтаФорма, Элемент);
	
КонецПроцедуры //Подключаемый_вики_КнопкаПриНажатии 

// По команде открыть форму обновления сайта
// 
&НаКлиенте
Процедура ОбновлениеОткрыть(Команда)
	
	вики_ДиалогКлиент.ФормаОбновлениеОткрыть(ЭтаФорма);
	
КонецПроцедуры

// По команде Перейти к обработке ошибки
// 
&НаКлиенте
Процедура ОшибкаПереход(Команда)
	
	Если НЕ СоединениеЕсть Тогда
		ИнформацияЗаполнитьСервер();
	ИначеЕсли НЕ НастройкиЕсть Тогда
		НастройкиОткрыть(Неопределено);
	ИначеЕсли НЕ СайтИнициализирован Тогда
		ИнициализацияОткрыть(Неопределено);
	ИначеЕсли ОбновлениеЕсть Тогда
		ОбновлениеОткрыть(Неопределено);
	КонецЕсли;
	
КонецПроцедуры

// По команде открыть форму пользователей сайта
// 
&НаКлиенте
Процедура ПользователиСайта(Команда)
	
	вики_ДиалогКлиент.ФормаСайтПользователиОткрыть(ЭтаФорма);
	
КонецПроцедуры

// По команде открыть настройки баннеров категорий
// 
&НаКлиенте
Процедура БаннерыКатегорийОткрыть(Команда)
	
	вики_ДиалогКлиент.ФормаБаннерыКатегорийОткрыть(ЭтаФорма);
	
КонецПроцедуры

// По команде очистить кэш
// 
&НаКлиенте
Процедура КэшОчистить(Команда)
	
	Если КэшОчиститьСервер() Тогда
		Предупреждение("Кэш сайта очищен.");
	КонецЕсли;
	
КонецПроцедуры

// По команде Обновить ссылки
// 
&НаКлиенте
Процедура СсылкиОбновить(Команда)
	
	вики_ДиалогКлиент.ФормаСсылкиОбновитьОткрыть(ЭтаФорма);
	
КонецПроцедуры

// Открытие формы управления файлами
//
&НаКлиенте
Процедура ФайлыУправление(Команда)
	
	вики_ДиалогКлиент.ФормаФайлыУправлениеОткрыть(ЭтаФорма);
	
КонецПроцедуры
